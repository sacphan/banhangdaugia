const express = require("express");
const exphbs = require("express-handlebars"),

    port = 4444,
    app = express();
const session = require("express-session");
const account = require("./controllers/AccountsC");
const mCat = require('./models/categoryM');
const mCatdel = require('./models/categorydetailM');
const mPro = require('./models/productM');
const bodyParser = require('body-parser');
const Handlebars=require("handlebars");

// const passport = require('passport');
Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(session({
    secret: '123',
    resave: false,
    saveUninitialized: true,
    cookie: {
        maxAge: 10000 * 50 * 5
    }
}))


//config express handlebar
const hbs = exphbs.create({
    defaultLayout: "layout",
    extname: 'hbs'
});

app.engine("hbs", hbs.engine);
app.set("view engine", "hbs");
//static file
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/area/public'));

app.use(async(req, res, next) => {
    cats = await mCat.all();
    for (let cat of cats) {
        cat.Detail = await mCatdel.getbyCatID(cat.CatID);
    }   
    next();
})
app.use('/',require("./controllers/usersAdminC"));
app.use("/",require("./controllers/productAdminC"));
app.use("/",require("./controllers/SellerC"));
app.use("/account", account);
app.use("/", require("./controllers/categoryC"));
app.use('/', require('./controllers/productC'));
app.use("/",require("./controllers/WatchListC"));  
app.use('/',require('./controllers/categoryAdminC'));
app.get('/', async(req, res) => {
    if (req.session.Role==3)
    {
        res.render('admin/home/index',{layout:"layoutadmin.hbs"});
    }
    else
    {
        const psNearExpried = await mPro.getNearlyExpired();
        const psTopNoAution = await mPro.getTopNoAution();
        const psTopPrice = await mPro.getTopPrice();
        if (req.session.user) res.render("home/index", {
            session: req.session,
            cats: cats,
            psNearExpried: psNearExpried,
            psTopNoAution: psTopNoAution,
            psTopPrice: psTopPrice
        });
        else {
            res.redirect("/account/login");


        }
    }
      
    })

    //listen to port 
app.listen(port);
console.log('server running on port 4444');
const express = require('express');
const router = express.Router();
const mCat = require('../models/categoryM');
const mPro = require('../models/productM');
const mCatdel = require('../models/categorydetailM');
router.get('/admin/products',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let products=await mPro.all();
    for (let i=0;i<products.length;i++)
    {
        products[i].STT=i+1;
    }
    res.render("admin/products",{
        layout:"layoutadmin.hbs",
        cats:cats,
        session:req.session,
        products:products
});
});
router.post('/admin/products/del',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let result =await mPro.del(req.body.ProID);
    res.json({result:result});
});
module.exports = router;
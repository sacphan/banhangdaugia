const express = require('express');
const router = express.Router();
const mCat = require('../models/categoryM');
const mPro = require('../models/productM');
const mHisPro=require("../models/history_auc_proM");
const accountM = require("../models/AccountsM");
const moment = require("moment");
const auctionListM=require("../models/AuctionLIstM");
const createAuM=require("../models/createAuctionM");
const requestauctionM=require("../models/requestauctionM");

// const moment=require("moment-precise-range-plugin");
router.get('/:id/detail', async(req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    const id = req.params.id;
    const ps = await mPro.getDetail(id);

    res.render('partials/DetailProduct', { title: 'Detail Product', layout: 'detailproduct', ps: ps, session: req.session });

});
router.get("/:page/search/products/:filter",async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
  
    const page=req.params.page;
    const ProName=req.query.ProName;
     const filter=req.params.filter;
     
    const result = await mPro.searchByProName(ProName,page,filter);
    const totalPage=result.pageTotal;
    const ps=result.rows;

  
    const pages=[];
    for (let i=0;i<totalPage;i++)
    {
        pages[i]={value: i+1,filter:filter,active: (i+1==page)?"active":""};
    }
    const navs={};
    if (page>1)
    {
        navs.prev=page-1;
    }
    if (page==1) navs.prev=1;
    if (page<totalPage)
    {
        navs.next=parseInt(page)+1;
    }

    if (page==totalPage) navs.next=page;
    res.render("products/productsSearch",{
        login: 1,
        cats: cats,
        ps: ps,
        session: req.session,       
        page: page,
        totalPage: totalPage,
        navs:navs,
        pages:pages,
        ProName:ProName,
        filter:filter
    
    });
   
})
router.get("/detail/products/:ProID",async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    const ProID=req.params.ProID;
    const psdel=await mPro.getDetail(ProID);
    psdel.User_Up_info=await accountM.getById(psdel.Up_User_id);
    psdel.User_Max_info=await accountM.getById(psdel.User_Max);
    const hispro=await mHisPro.getByProID(psdel.ProID);
    const auctionList=await auctionListM.all(req.session.user);
    const productsRelate=await mPro.getProductsRelate(psdel.CatID,psdel.CatDelID,psdel.ProID);
    const currentUser=await accountM.getById(req.session.user);
    if (currentUser.Scored_Report>=8) psdel.Bid=true;
    const Bit_Price=await createAuM.getBitPrice(ProID)
    const requestauction=await requestauctionM.getByProID(ProID);
    for (let i=0;i<requestauction.length;i++)
    {
        requestauction[i].STT=i+1;
    }
    psdel.Price=parseInt(psdel.Bit_Price)+parseInt(psdel.Price);
    
    res.render("products/detailproducts",{
        login: 1,
        cats: cats,
        session: req.session,
        products:psdel,
        productsRelate:productsRelate,
        hispro:hispro,
        auctionList:auctionList,
        requestauction:requestauction
       
    })
})
router.get("/products/placebid",async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    res.redirect("/");
})
router.post("/products/requestaution/duyet",async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let ID_SELLER=req.body.sellerid;
    let ID_USER=req.body.userid;
    let ID_PRO=req.body.proid;
    
    await requestauctionM.del(ID_SELLER,ID_USER,ID_PRO);
    let user ={
        f_ID:ID_USER,
        Scored_Report:8
    }
    let result=await accountM.update(user);
    res.json({result:result});
});
router.post("/products/requestaution/tuchoi",async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let ID_SELLER=req.body.sellerid;
    let ID_USER=req.body.userid;
    let ID_PRO=req.body.proid;
    await requestauctionM.del(ID_SELLER,ID_USER,ID_PRO);
    res.json({result:result});
});

router.get("/products/request_placebid/:ID_SELLER/:PROID/:PRONAME",async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let ID_SELLER=req.params.ID_SELLER;
    let PROID=req.params.PROID;
    let PRONAME=req.params.PRONAME;
    const requestauction={
        ID_USER:req.session.user,
        ID_SELLER:ID_SELLER,
        ID_PRO:PROID,
        PRONAME:PRONAME
    }
    let result = await requestauctionM.add(requestauction);
    res.json({result:result});
});
router.post("/products/placebid",async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let placebid=req.body.placebid;
    let ProID=req.body.ProID;
    let psdel=await mPro.getDetail(ProID); 
    let error;
    let nowdate=moment().format("DD-MM-YYYY hh:mm:ss");
    nowdate=moment(nowdate,"DD-MM-YYYY hh:mm:ss");
    let Dead_time=psdel.Dead_time;
    Dead_time=moment(Dead_time,"DD-MM-YYYY hh:mm:ss");
    let diff=Dead_time.diff(nowdate);
    diffminutes=diff/(1000*60*60);
   
    if (placebid>psdel.Price && diffminutes>0)
    {
        let product;
        if (psdel.AUTO==1 && diffminutes<=5)
        {
          
            Dead_time=moment(Dead_time).add(10,"minutes");
           
            product={
                ProID:ProID,
                Price:placebid,
                NoAuction:parseInt(psdel.NoAuction)+1,
                Dead_time:moment(Dead_time).format("DD-MM-YYYY hh:mm:ss")
            }
        }
        else
        {
            product={
                ProID:ProID,
                Price:placebid,
                NoAuction:parseInt(psdel.NoAuction)+1
            }
        }
        
        let result=await mPro.update(product);
        
        if (result)
        {
            error="Đấu giá thành công";
            let hisacs={
                    TIME:moment().format("YYYY-MM-DD hh:mm:ss"),
                    USER:req.session.username,
                    ID_PRO:ProID,
                    PRICE:placebid,
                    UserID:req.session.user
            }
            await mHisPro.add(hisacs);
        }
        else{
            error="Đấu giá thất bại";
        }
      
    }
    else{
        error="Đấu giá thất bại";
    }
    psdel=await mPro.getDetail(ProID); 
    psdel.User_Up_info=await accountM.getById(psdel.Up_User_id);
    psdel.User_Max_info=await accountM.getById(psdel.User_Max);
    const hispro=await mHisPro.getByProID(psdel.ProID);
    const auctionList=await auctionListM.all(req.session.user);
    const productsRelate=await mPro.getProductsRelate(psdel.CatID,psdel.CatDelID,psdel.ProID);
    const currentUser=await accountM.getById(req.session.user);
    if (currentUser.Scored_Report>=8) psdel.Bid=true;
    const Bit_Price=await createAuM.getBitPrice(ProID)
    psdel.Price=parseInt(psdel.Bit_Price)*2+parseInt(psdel.Price);
    res.render("products/detailproducts",{
        login: 1,
        cats: cats,
        session: req.session,
        products:psdel,
        productsRelate:productsRelate,
        hispro:hispro,
        auctionList:auctionList,
        error:error
        
       
    })
});
module.exports = router;
const express = require('express');
const router = express.Router();
const bcrypt = require("bcryptjs");
const accountM = require("../models/AccountsM");
const watchlistM=require("../models/WatchListM");
const AuctionM=require("../models/AuctionLIstM");
const evaluateM=require("../models/evaluate_detailM");
const auc_list_winM=require("../models/auc_list_winM");
const moment = require("moment");
const nodemailer =  require('nodemailer');
// router.use(passport.initialize());
// router.use(passport.session());
// router.use(require('../config/passport')(passport));
router.use(async(req, res, next) => {
    if (req.session.user) {
        
        watchlist = await watchlistM.all(req.session.user);  
        auctionlist=await AuctionM.all(req.session.user);
    }
    
    next();
})
router.get("/createAccount", function(req, res) {
    res.render("account/createAccount", { layout: false });
})
router.get("/login", function(req, res) {
    if (req.session.user) res.redirect('/');
    else {
        res.render("account/login", { layout: false });
    }

})
router.get('/register', (req, res) => {
    res.redirect('/');
})

router.post('/login', async(req, res) => {
        const username = req.body.username;
        const password = req.body.password;
     
        const user = await accountM.getByUsername(username);
        if (user === null) {
            res.render('account/login', { error: 'Username không tồn tại!', layout: false });
            return false;
        }
        bcrypt.compare(password, user.f_Password, function(err, result) {
            if (result == true) {

                req.session.user = user.f_ID;
                req.session.email = user.f_Email;
                req.session.username = user.f_Username;
                req.session.Role=user.Role;           
                res.redirect('/');
                return true;
            } else {
                res.render('account/login', { error: 'Mật khẩu của bạn không đúng!', layout: false });
                return false;
            }

        }); // true




    })
    // router.post('/createAccount', passport.authenticate('local-signup', {
    //         successRedirect: '/',
    //         failureRedirect: '/account/createAccount',
    //         failureFlash: true
    //     }))
router.post('/createAccount', async(req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    var user;
    bcrypt.hash(password, bcrypt.genSaltSync(10), function(err, hash) {

        user = {
            f_ID: null,
            f_Username: username,
            f_Password: hash,
            f_Name: req.body.name,
            f_Email: req.body.email,
            f_DOB: req.body.DOB,
            f_Permission: 0
        };
    });

    const uEmail = await accountM.getByEmail(req.body.email);
    if (uEmail != null) {

        res.render('account/createAccount', { error: "Email đã tồn tại", layout: false, user: user });
        return false;
    }

    const uId = await accountM.add(user);

    if (uId) res.redirect('/account/login');
    else {
        res.render('account/createAccount', { error: "Đăng nhập thất bại", layout: false, user: user });
        return false;
    }
})

router.get('/profile', async(req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    
        let profile = await accountM.getByEmail(req.session.email);
        let listevaluate=await evaluateM.getbyUserID(req.session.user);
        let auc_list_win =await  auc_list_winM.getbyUserID(req.session.user);
        res.render('account/profile', {
            listevaluate:listevaluate,
            auctionlist:auctionlist,
            watchlist:watchlist, 
            profile: profile, 
            session: req.session, 
            cats: cats,
            scored:profile.Scored_Report,
            auc_list_win:auc_list_win
         });
        return true;
})
router.post('/profile/info', async(req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    const Name=req.body.first_name;
    const Phone=req.body.phone;
    const Email=req.body.email;
    let DOB=req.body.DOB;
    const user={
        f_ID:req.session.user,
        f_Name:Name,
        Phone:Phone,
        f_Email:Email,
        f_DOB:DOB
    }
    const resultinfo=await accountM.update(user);
    res.redirect("/account/profile");
})
router.post('/profile/changepassword', async(req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    const password = req.body.password;
    const newpassword = req.body.newpassword;
    const confirmnewpassword = req.body.confirmnewpassword;
    let profile = await accountM.getByEmail(req.session.email);
    if (newpassword!=confirmnewpassword)
    {
    
        res.render('account/profile', {auctionlist: auctionlist,watchlist:watchlist, profile: profile, session: req.session, cats: cats,error:"Mật khẩu xác nhận không đúng" });
        return false;
    }
    bcrypt.compare(password, profile.f_Password,  async(err, result)=> {
        if (result == true) {
            
            bcrypt.hash(newpassword, bcrypt.genSaltSync(10), async(err, hash)=> {

                user = {
                    f_ID: req.session.user,                  
                    f_Password: hash,                 
                };
                const update = await accountM.update(user);
                if (update)
                {
                    res.render('account/profile', {auctionlist:auctionlist,watchlist:watchlist,profile: profile, session: req.session, cats: cats, success: 'Mật khẩu đổi thành công!' });
                    return true;
                } 
                else return false;
            });
           
        } else {
            res.render('account/profile', { watchlist:watchlist,profile: profile, session: req.session, cats: cats,error: 'Mật khẩu hiện tại không đúng!' });
            return false;
        }

    }); // true
})


router.get('/logout', (req, res) => {
    if (req.session) {
        //delete sesstion object
        req.session.destroy(function(err) {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        })
    }
})
router.post("/evaluate_list_win",async (req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let UserID=req.body.UserID;
    let SellerID=req.body.SellerID;
    let ProID=req.body.ProID;
    let Comment=req.body.Comment;
    let Scored=req.body.Scored;
    let account=await accountM.getById(SellerID);
    account.Scored_Report=parseInt(Scored)+parseInt(account.Scored_Report);
    result =await accountM.update(account);
    if (result)
    {   
        list={
            ID_USER:UserID,
            ID_SELLER:SellerID,
            ID_PRO:ProID,
            RPORTS:Comment,
            Scored:Scored
        }
        result=await auc_list_winM.update(list);
        
        return res.json({Comment:Comment,Scored:Scored});
    }
    return res.json({result:false});
})
router.get("/forgotpassword/:id",async (req,res)=>{
    const id=req.params.id;
    res.render("account/reserpassword",{layout:false,id:id});
});
router.post("/resetPassword",async (req,res)=>{
    const newpassword=req.body.newpassword;
    const confirmnewpassword=req.body.confirmnewpassword;
    const id=req.body.id;
    
    var user;
    if (newpassword==confirmnewpassword)
    {
        bcrypt.hash(newpassword, bcrypt.genSaltSync(10), async function(err, hash) {

            user = {
                f_ID: id,              
                f_Password: hash,              
            };
            let result=await accountM.update(user);
        });
       
      
        res.redirect('/account/login');
        return;
    }
    res.render("account/reserpassword",
    {
        layout:false,id:id,
        error:"Reser mật khẩu thất bại!"
    })
})
router.post("/forgotpassword",async (req,res)=>{
    const email=req.body.email;
    const user=await accountM.getByEmail(email);
    
    var transporter =  nodemailer.createTransport({ // config mail server
      
        service: 'Gmail',
        auth: {
            user: 'daugiahanghoa24h@gmail.com',
            pass: 'Daugia24h'
        }
    });
    var mainOptions = { // thiết lập đối tượng, nội dung gửi mail
        from: 'daugiahanghoa24h@gmail.com',
        to: `${email}`,
        subject: 'Recover Password',
        text: 'You recieved message from ' + req.body.email,
        html: `<a href="http://localhost:4444/account/forgotpassword/${user.f_ID}">Click here change password</a>`
    }
    let info=await transporter.sendMail(mainOptions, function(err, info){
        if (err) {
            console.log(err);
            res.redirect('/');
        } else {
            console.log('Message sent: ' +  info.response);
            res.redirect('/');
        }
    });
    res.redirect('/account/login');
});
//admin


module.exports = router;
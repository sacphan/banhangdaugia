const express = require('express');
const router = express.Router();
const watchlistM=require('../models/WatchlistM');
router.get("/watchlist/:ProID/:ProName",async(req,res)=>
{
    const ProID=req.params.ProID;
    const ProName=req.params.ProName;
    const result=await watchlistM.saveWatchlist(ProID,ProName,req.session);
    return res.json({result:result});
})
module.exports = router;
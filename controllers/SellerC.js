const express = require('express');
var http = require('http');
var fs = require('fs');
const mPro = require('../models/productM');
const router = express.Router();
const accountM = require("../models/AccountsM");
const upgrade_seller = require("../models/upgrade_sellerM");
const formidable = require("formidable");
const imageproductsM = require("../models/imageproductsM");
const auc_list_winM = require("../models/auc_list_winM");
const historyM = require("../models/history_auc_proM");
const moment = require("moment");
const moveFile = require('move-file');
router.get('/upseller/:ID_User/:Name/:Scored', async (req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    const ID_User = req.params.ID_User;
    const Name = req.params.Name;
    const Scored = req.params.Scored;
    const seller = {
        ID_User: ID_User,
        Name: Name,
        Scored_Report: Scored
    }
    if (req.session.Role == 2) {
        return res.json({ result: -1 });
    }
    let result = await upgrade_seller.add(seller)
    return res.json({ result: result });
})
router.get("/seller", async (req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }

    if (req.session.Role == 1)
        return res.render("seller/index", {
            notify: "Bạn không phải seller hãy gửi yêu cầu đến admin để được trở thành seller!",
            session: req.session,
            cats: cats
        });
    else {
        const listproducts = await mPro.getListBySellerID(req.session.user);
        const listwin = await auc_list_winM.getbySellerID(req.session.user);
        return res.render("seller/index", {
            session: req.session,
            cats: cats,
            products: listproducts,
            listwin: listwin
        });
    }
});
router.get("/createproduct", async (req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    if (req.session.Role == 2)
        return res.render("seller/createproduct",
            {
                cats: cats,
                session: req.session
            });
    else {
        return res.render("seller/createproduct",
            {
                cats: cats,
                session: req.session,
                notify: "Bạn không phải seller hãy gửi yêu cầu đến admin để được trở thành seller!"
            });
    }
});
function getExtension(filename) {
    return filename.split('.').pop();
}

router.post("/createproduct", async (req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }

    let form = new formidable.IncomingForm();
    // Cấu hình thư mục sẽ chứa file trên server với hàm .uploadDir
    form.uploadDir = `Public/Images/products/`;
    form.keepExtensions = true;
    form.maxFieldsSize = 10 * 1024 * 1028;
    form.multiples = true;
    // Xử lý upload file với hàm .parse
  
    form.parse(req, async (err, fields, files) => {

        if (err) {

            res.render("seller/createproduct", {
                cats: cats,
                session: req.session,
                error: "Lỗi upload hình ảnh!"
            })

            return;
        };
        let products = {
            Up_User_id: req.session.user,
            ProName: fields.proname,
            CatID: fields.catid,
            Price: fields.price,
            CatDelID: fields.catdelid,
            Bit_Price: fields.bit_price,
            FullDes: fields.FullDes,
            AUTO: fields.auto,
            Up_Pro: moment().format("YYYY-MM-DD hh:mm:ss"),
            Dead_time: moment(fields.dead_time).format("YYYY-MM-DD hh:mm:ss")
        }
        let id = await mPro.add(products);
        var arrayFile = files;
        fs.mkdir(`./Public/Images/products/${id}`, function (err) {
            if (err) {
                return console.error(err);
            }
          
        });
        
        let link = form.uploadDir;
        let filename = [];
        if (arrayFile.files.length > 0) {

            let i = 0;
            arrayFile.files.forEach(async element => {
                i = i + 1;
                let name = element.path.split('\\');
                filename.push(name[name.length - 1]);
                oldpath = link + "\\" + name[name.length - 1];
                newPath = link + `/${i}.` + 'jpg';
                fs.rename(oldpath, newPath, (err) => {
                    if (err) throw err;
                });
                moveold=newPath;
                movenew=link + `\\${id}\\` + `/${i}.` +'jpg';
               
                await moveFile(moveold,movenew);

            });


        }
       
        // filename.forEach(async element => {
        //     let imageproducts = {
        //         PROID: id,
        //         PRONAME: fields.proname,
        //         IMGNAME: element
        //     }
        //     idIMG = await imageproductsM.add(imageproducts);
        // })

      


    
            // res.render("seller/createproduct",{
            //     cats:cats,
            //     session:req.session,
            //     error:"Up load thất bại"              
            // });

        // Lấy ra đường dẫn tạm của tệp tin trên server
      
    });


res.redirect("/seller");
   
   
    
});
router.get("/Images/:Name", function (req, res) {
    IMGNAME = req.params.Name;
    res.render(`Images/products/${IMGNAME}`);
})
router.post('/seller/addcomment', async (req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let comment = req.body.FullDes;
    let addcomment = req.body.commentadd;
    let ProID = req.body.ProID;
    let FullDes = comment + "\n\n" + moment().format("DD-MM-YYYY hh:mm:ss") + "\n\n" + addcomment;
    let product = {
        ProID: ProID,
        FullDes: FullDes
    }
    var result = await mPro.update(product);
    res.redirect(`/detail/products/${ProID}`);

})
router.post('/product/refuseAucs', async (req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    const UserID = req.body.userid;
    const ProID = req.body.proid;

    const histauc = {
        UserID: UserID,
        ID_PRO: ProID,
        RoleAucs: 0
    }
    const result = await historyM.update(histauc);
    const hispro = await historyM.getByProID(psdel.ProID);
    let product = {
        ProID: ProID,
        Price: hispro[hispro.length - 1].Price
    }
    result = await mPro.update(product);
    res.redirect(`/detail/products/${ProID}`);
});
router.post("/cacelwin", async (req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }

    let UserID = req.body.UserID;
    let SellerID = req.body.SellerID;
    let ProID = req.body.ProID;
    let Comment = req.body.Comment;
    let Scored = req.body.Scored;
    let account = await accountM.getById(UserID);
    account.Scored_Report = parseInt(Scored) + parseInt(account.Scored_Report);


    result = await accountM.update(account);
    if (result) {

        result = await auc_list_winM.del(UserID, SellerID, ProID);

        return res.json({ result: true });
    }
    return res.json({ result: false });
})
router.post("/save_history_win", async (req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let UserID = req.body.UserID;
    let SellerID = req.body.SellerID;
    let ProID = req.body.ProID;
    let Comment = req.body.Comment;
    let Scored = req.body.Scored;
    let account = await accountM.getById(UserID);
    account.Scored_Report = parseInt(Scored) + parseInt(account.Scored_Report);
    result = await accountM.update(account);
    if (result) {
        list = {
            ID_USER: UserID,
            ID_SELLER: SellerID,
            ID_PRO: ProID,
            RPORTS: Comment,
            Scored: Scored
        }
        result = await auc_list_winM.update(list);

        return res.json({ Comment: Comment, Scored: Scored });
    }
    return res.json({ result: false });
})
module.exports = router;
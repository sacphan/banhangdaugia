const express = require('express');
const router = express.Router();
const upgradersellerM=require("../models/upgrade_sellerM");
const accountM=require("../models/AccountsM");
const RoleM=require("../models/RoleM");
router.get('/admin/users',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let account=await accountM.all();
    const role=await RoleM.all();
    for (let i=0;i<account.length;i++)
    {
        account[i].STT=i+1; 
        account[i].Rolelist=role;
    }
    
    res.render("admin/users",{
        layout:"layoutadmin.hbs",
        cats:cats,
        session:req.session,
        account:account,
       
});
});
router.post('/admin/user/edit',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let user={
        f_ID:req.body.userid,
        Role:req.body.role
    }
    
    let result =await accountM.update(user);
    
    res.json({result:result});
});
router.get('/admin/upgradeseller',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let upgraderseller=await upgradersellerM.all();
    for (let i=0;i<upgraderseller.length;i++)
    {
        upgraderseller[i].STT=i+1; 
    }
    res.render("admin/upgradeseller",{
    layout:"layoutadmin.hbs",
    cats:cats,
    session:req.session,
    upgraderseller:upgraderseller}
    )
});
router.post('/admin/upgradeseller/duyet',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let id=req.body.userid;
    await upgradersellerM.del(id);
    let user ={
        f_ID:id,
        Role:2
    }
    let result=await accountM.update(user);
    res.json({result:result});

});
router.post('/admin/upgradeseller/tuchoi',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let id=req.body.userid;
    let result= await upgradersellerM.del(id);
    res.json({result:result});

});
module.exports = router;
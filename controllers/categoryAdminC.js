const express = require('express');
const router = express.Router();
const mCat = require('../models/categoryM');
const mPro = require('../models/productM');
const mCatdel = require('../models/categorydetailM');
router.get('/admin/category',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    for (let i=0;i<cats.length;i++)
    {
        cats[i].STT=i+1;
    }
    res.render("admin/category",{
        layout:"layoutadmin.hbs",
        cats:cats,
        session:req.session
});
})
router.post('/admin/category/edit',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let category={
        CatID:req.body.CatID,
        CatName:req.body.CatName
    }
    let result =await mCat.update(category);
    
    res.json({result:result});
})
router.post('/admin/category/add',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let category={
      
        CatName:req.body.CatName
    }
    let result =await mCat.add(category);
    res.json({result:result});
});
router.post('/admin/category/del',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let result =await mCat.del(req.body.catid);
    res.json({result:result});
});
router.get('/admin/categorydetail/:CatID',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let catDetail=await mCatdel.getbyCatID(req.params.CatID);
    for (let i=0;i<catDetail.length;i++)
    {
        catDetail[i].STT=i+1;
    }
    res.render("admin/categorydetail",{
        layout:"layoutadmin.hbs",
        cats:cats,
        session:req.session,
        catDetail:catDetail,
        CatID:req.params.CatID
});
});

router.post('/admin/categorydetail/edit',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let category={
        ID:req.body.id,
        Name:req.body.name
    }
    
    let result =await mCatdel.update(category);
    
    res.json({result:result});
})
router.post('/admin/categorydetail/add',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let category={
        CatID:req.body.CatID,
        Name:req.body.name
    }
    let result =await mCatdel.add(category);
    res.json({result:result});
});
router.post('/admin/categorydetail/del',async(req,res)=>{
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    let result =await mCatdel.del(req.body.id);
    res.json({result:result});
});
module.exports = router;
const express = require('express');
const router = express.Router();
const mCat = require('../models/categoryM');
const mPro = require('../models/productM');


router.get('/:id/:iddel/products', async(req, res) => {
    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    const id = req.params.id;
    const iddel = req.params.iddel;
    const page = 1;
    const ps = await mPro.allByCatId(id, iddel);
    res.render("products/products", {
        login: 1,
        username: req.session.username,
        cats: cats,
        ps: ps,
        session: req.session
    })
})

router.get('/:id/:iddel/:page/products', async(req, res) => {

    if (!req.session.user) {
        res.redirect("/account/login");
        return false;
    }
    const id = req.params.id;
    const iddel = req.params.iddel;
    const page = req.params.page;
    const ps = await mPro.allByCatId(id, iddel, page);
    const totalPage = await mPro.getpageTotal(id);
    const pages=[];
    for (let i=0;i<totalPage-1;i++)
    {
        pages[i]={value: i+1,active: (i+1==page)?"active":""};
        pages[i].CatID=id;
        pages[i].ID=iddel;
    }
    const navs={};
    if (page>1)
    {
        navs.prev=page-1;
    }
    if (page==1) navs.prev=1;
    if (page<totalPage)
    {
        navs.next=parseInt(page)+1;
    }
    if (page==totalPage) navs.next=page;
    res.render("products/products", {
        login: 1,
        username: req.session.username,
        cats: cats,
        ps: ps,
        session: req.session,
        CatID: id,
        ID: iddel,
        page: page,
        totalPage: totalPage,
        navs:navs,
        pages:pages
    });


})
module.exports = router;
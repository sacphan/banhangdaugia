const db = require('../utils/db');
const tbName = 'evaluate_detail';
const moment = require("moment");
module.exports = {
    getbyUserID: async UserID => {
        const sql = `SELECT * FROM ${tbName}  WHERE UserID=${UserID}`;
        const rows = await db.load(sql);
        for (let i=0;i<rows.length;i++)
        {
            rows[i].STT=i+1;
        }
        return rows;
    }
}
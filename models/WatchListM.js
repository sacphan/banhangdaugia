const db = require("../utils/db.js");
const tbName = "watchlist";
const mysql = require("mysql");



module.exports = {
  all: async(ID_User) => {
    
    const sql = `SELECT * FROM ${tbName} WHERE ID_User=${ID_User}`;
    const rows =await db.load(sql);
    return rows;
},
    saveWatchlist: async (ProID,ProName,session) => {
       let watchlist={
            Name_User: session.username,
            ID_User: session.user,
            ID_Pro:ProID,
            Name_Pro:ProName
       }
      let result=await db.add(tbName,watchlist);
      console.log(result);
      return result;
    }
}

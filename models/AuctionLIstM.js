const db = require("../utils/db.js");
const tbName = "auction_list";
const idField="f_ID";
const mysql = require("mysql");
const moment = require("moment");
module.exports = {
    all: async(ID_User) => {
        console.log(ID_User);
        const sql = `SELECT * FROM ${tbName} WHERE ID_USER=${ID_User}`;
        const rows =await db.load(sql);
        for (let i = 0; i < rows.length; i++) {
            rows[i].Up_Pro = moment(rows[i].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
            rows[i].Dead_Time = moment(rows[i].Dead_Time).format("DD-MM-YYYY hh:mm:ss");
        }
        return rows;
    }
}
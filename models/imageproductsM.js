const db = require("../utils/db.js");
const tbName = "imageproducts";

module.exports = {
    add: async imageproducts => {
        const id = await db.add(tbName, imageproducts);
        return id;
    }
}
const db = require("../utils/db.js");
const tbName = "create_auction";
const idField="f_ID";
const mysql = require("mysql");
const moment = require("moment");
module.exports = {
    getBitPrice: async (ID_PRO) => {
        const sql = `SELECT * FROM ${tbName} WHERE  ID_Pro=${ID_PRO}`;
        const rows = await db.load(sql);      
        if (rows[0])
            return rows[0].Bit_Price;
        else return null;
    }
}
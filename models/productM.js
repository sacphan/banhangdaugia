const db = require('../utils/db');
const tbName = 'Products';
const moment = require("moment");
const momentdiff=require("moment-precise-range-plugin");
const idField="ProID";
module.exports = {
    add: async user => {
        const id = await db.add(tbName, user);
        return id;
    },
    all: async() => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },
    del:async (ProID)=>{
        const sql=`DELETE FROM ${tbName} WHERE ProID =${ProID}`;
        const rows=await db.load(sql);
    
        return rows;
    },
    update:async entity=>{
        const resultinfo=await db.update(tbName,idField,entity);
        return resultinfo;
       
    },
    allByCatId: async(CatId, CatDelID, Page) => {     
        sql = `SELECT * FROM ${tbName} Where CatID=${CatId} AND CatDelID=${CatDelID}  ORDER BY Dead_time DESC  LIMIT 6 OFFSET ${(Page-1)*6}`;
        const rows = await db.load(sql);
        for (let i = 0; i < rows.length; i++) {
            rows[i].Up_Pro = moment(rows[i].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
            rows[i].Dead_time = moment(rows[i].Dead_time).format("DD-MM-YYYY hh:mm:ss");
        }           
        return rows;
    },

    getDetail: async(ProId) => {
        const sql = `select * from ${tbName} where ${ProId}=ProID`;
        const rows = await db.load(sql);
        rows[0].Up_Pro = moment(rows[0].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
        rows[0].Dead_time = moment(rows[0].Dead_time).format("DD-MM-YYYY hh:mm:ss");      
        return rows[0];
    },
    getListBySellerID:async(SellerID)=>{
        const sql = `select * from ${tbName} where ${SellerID}=Up_User_id AND CURRENT_TIMESTAMP()<Dead_time`;
        const rows = await db.load(sql);
        for (let i = 0; i < rows.length; i++) {
            rows[i].Up_Pro = moment(rows[i].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
            rows[i].Dead_time = moment(rows[i].Dead_time).format("DD-MM-YYYY hh:mm:ss");
        }           
        return rows;
    },
    getpageTotal:async(CatId)=>{
        let sql=`SELECT * FROM ${tbName} Where CatID=${CatId}`;
        const rs=await db.load(sql);
        const totalP=rs.length;
        const pageTotal=Math.floor(totalP/6)-1;
        return pageTotal;
    },
    getProductsRelate:async(CatID,CatDelID,ProID)=>{
        sql = `SELECT * FROM ${tbName} Where CatID=${CatID} AND CatDelID=${CatDelID} AND ProID!=${ProID}  ORDER BY Dead_time DESC  LIMIT 5`;
        const rows = await db.load(sql);
        for (let i = 0; i < rows.length; i++) {
            rows[i].Up_Pro = moment(rows[i].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
            rows[i].Dead_time = moment(rows[i].Dead_time).format("DD-MM-YYYY hh:mm:ss");
        }           
        return rows;
    },
    getNearlyExpired: async() => {
        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = date_ob.getMonth() + 1;
        let year = date_ob.getFullYear();
        let datetimecurrent = year + "-" + month + "-" + date;
        const sql = `SELECT * FROM ${tbName} WHERE '${datetimecurrent}'<Dead_time LIMIT 5`;
        const rows = await db.load(sql);
        for (let i = 0; i < rows.length; i++) {
            rows[i].Up_Pro = moment(rows[i].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
            rows[i].Dead_time = moment(rows[i].Dead_time).format("DD-MM-YYYY hh:mm:ss");
        }
        return rows;
    },
    getTopNoAution: async() => {
        const sql = `SELECT  * FROM ${tbName} ORDER BY NoAuction DESC LIMIT 5`;
        const rows = await db.load(sql);
        for (let i = 0; i < rows.length; i++) 
        {
            rows[i].Up_Pro = moment(rows[i].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
            rows[i].Dead_time = moment(rows[i].Dead_time).format("DD-MM-YYYY hh:mm:ss");
        }
        return rows;
    },
    getTopPrice: async() => {
        const sql = `SELECT  * FROM ${tbName} ORDER BY Price DESC LIMIT 5`;
        const rows = await db.load(sql);
        for (let i = 0; i < rows.length; i++) 
        {
            rows[i].Up_Pro = moment(rows[i].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
            rows[i].Dead_time = moment(rows[i].Dead_time).format("DD-MM-YYYY hh:mm:ss");
        }
        return rows;
    },
    searchByProName:async(ProName,Page,filter)=>{
        let sql=`SELECT * FROM products WHERE MATCH (ProName,TinyDes,FullDes) AGAINST ('${ProName}'IN BOOLEAN MODE)`;
        const rs=await db.load(sql);
        const totalP=rs.length;
        const pageTotal=Math.floor(totalP/6)+1;
        if (filter==1)
        {
            sql=`SELECT * FROM products WHERE MATCH (ProName,TinyDes,FullDes) AGAINST ('${ProName}'IN BOOLEAN MODE) ORDER BY Dead_time DESC  LIMIT 6 OFFSET ${(Page-1)*6}`;
        }
        else if (filter==2)
        {
            sql=`SELECT * FROM products WHERE MATCH (ProName,TinyDes,FullDes) AGAINST ('${ProName}'IN BOOLEAN MODE) ORDER BY Price ASC  LIMIT 6 OFFSET ${(Page-1)*6}`;
        }
        else
        {
            sql=`SELECT * FROM products WHERE MATCH (ProName,TinyDes,FullDes) AGAINST ('${ProName}'IN BOOLEAN MODE) ORDER BY Up_Pro DESC  LIMIT 6 OFFSET ${(Page-1)*6}`;
        }        
        const rows=await db.load(sql);
        var nowdate=moment().format("YYYY-MM-DD hh:mm:ss").toString()
       
        

       
        for (let i = 0; i < rows.length; i++) 
        {
            let Up_pro=moment(rows[i].Up_Pro).format("YYYY-MM-DD hh:mm:ss").toString();
            let diff=moment.preciseDiff(Up_pro,nowdate,true);
            if (diff.years==0 && diff.months==0 && diff.days==0 && diff.minutes<30) rows[i].new=1;
            rows[i].Up_Pro = moment(rows[i].Up_Pro).format("DD-MM-YYYY hh:mm:ss");
            rows[i].Dead_time = moment(rows[i].Dead_time).format("DD-MM-YYYY hh:mm:ss");
        }
        return {rows:rows,pageTotal:pageTotal};
    }
}
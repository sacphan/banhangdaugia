const db = require('../utils/db');
const tbName = 'Role';
module.exports = {
    all: async() => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },
    getRolebyID: async(ID) => {
        const sql = `SELECT * FROM ${tbName} where ID=${ID}`;
        const rows = await db.load(sql);
        return rows;
    }
}
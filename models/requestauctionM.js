const db = require("../utils/db.js");
const tbName = "requestauction";
const idField="ID";
const mysql = require("mysql");
const moment = require("moment");
module.exports = {
    add: async requestauction => {
        const id = await db.add(tbName, requestauction);
        return id;
    },
    all: async() => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },
    del:async (ID_SELLER,ID_USER,ID_PRO)=>{
        const sql=`DELETE FROM ${tbName} WHERE ID_SELLER = ${ID_SELLER} AND ID_USER=${ID_USER} AND ID_PRO= ${ID_PRO}`;
        console.log(sql);
        const rows=await db.load(sql);
    
        return rows;
    },
    getByProID:async (ProID)=>{
        const sql=`SELECT * FROM ${tbName} WHERE ID_PRO =${ProID}`;
        const rows=await db.load(sql);
    
        return rows;
    }
}
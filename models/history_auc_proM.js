const db = require('../utils/db');
const tbName = 'history_auc_pro';
const moment = require("moment");
const idField1="UserID";
const idField2="ID_PRO";
module.exports = {
    add: async history_auc => {
        const id = await db.add(tbName, history_auc);
        return id;
    },
    getByProID: async(ID_PRO) => {
        const sql = `SELECT * FROM ${tbName} WHERE ID_PRO=${ID_PRO} AND RoleAucs=1 ORDER BY PRICE DESC`;
        const rows = await db.load(sql);
        for (let i = 0; i < rows.length; i++) {
            rows[i].TIME = moment(rows[i].TIME).format("DD-MM-YYYY hh:mm:ss");       
            let split=rows[i].USER.split(' ');
            rows[i].USER="***"+split[split.length-1];  
        }  
        return rows;
    },
    del:async (ID_PRO,UserID)=>{
        const sql=`DELETE FROM ${tbName} WHERE ID_PRO =${ID_PRO} AND UserID=${UserID}`;
        const rows=await db.load(sql);
    
        return rows;
    },
    update: async history_auc => {
        const id = await db.update2(tbName,idField1,idField2, history_auc);
        return id;
    },
}
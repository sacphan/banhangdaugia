const db = require('../utils/db');
const tbName = 'auc_list_win';
const moment = require("moment");
const idField1="ID_USER";
const idField2="ID_PRO";
module.exports = {
    getbyUserID: async UserID => {
        const sql = `SELECT * FROM ${tbName}  WHERE ID_USER=${UserID}`;
        const rows = await db.load(sql);
        for (let i=0;i<rows.length;i++)
        {
            rows[i].STT=i+1;
        }
       
        return rows;
    },
    getbySellerID: async UserID => {
        const sql = `SELECT * FROM ${tbName}  WHERE ID_SELLER=${UserID}`;
        const rows = await db.load(sql);
        for (let i=0;i<rows.length;i++)
        {
            rows[i].STT=i+1;
        }
       
        return rows;
    },
    update:async entity=>{
        const resultinfo=await db.update2(tbName,idField1,idField2,entity);
        return resultinfo;
       
    },
    del:async (ID_USER,ID_SELLER,ID_PRO)=>{
        const sql=`DELETE FROM ${tbName} WHERE ID_PRO =${ID_PRO} AND ID_USER=${ID_USER} AND ID_SELLER=${ID_SELLER}`;
        const rows=await db.load(sql);
    
        return rows;
    },
}
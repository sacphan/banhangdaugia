const db = require('../utils/db.js');
const tbName = 'Categories';
const idField="CatID";
module.exports = {
    all: async() => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows =await db.load(sql);
        return rows;
    },
    add: async category => {
        const id = await db.add(tbName, category);
        return id;
    },
    del:async (CatID)=>{
        const sql=`DELETE FROM ${tbName} WHERE CatID =${CatID}`;
        const rows=await db.load(sql);
    
        return rows;
    },
    update: async category => {
        const id = await db.update(tbName,idField, category);
        return id;
    },

}
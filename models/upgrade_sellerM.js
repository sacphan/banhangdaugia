const db = require("../utils/db.js");
const tbName = "upgrade_seller";
const idField="f_ID";
const mysql = require("mysql");
const moment = require("moment");
module.exports = {
    add: async upgrade_seller => {
        const id = await db.add(tbName, upgrade_seller);
        return id;
    },
    all: async() => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },
    del:async (ID)=>{
        const sql=`DELETE FROM ${tbName} WHERE ID_User =${ID}`;
        const rows=await db.load(sql);
    
        return rows;
    },
}
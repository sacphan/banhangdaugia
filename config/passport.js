const localStrategy = require('passport-local').Strategy;
const accountM = require("../models/AccountsM");

module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.email);
    })

    passport.deserializeUser(function(email, done) {
        accountM.getByEmail(email, function(err, user) {
            done(err, user.email);
        })
    })

    passport.use('local-sigup', new localStrategy({
        newUser,
        passReqToCallback: true
    }, function(req, newUser, done) {
        process.nextTick(function() {
            accountM.getByEmail(newUser.email, function(err, user) {
                if (err) return done(err);
                if (user) return done(null, false, req.flash('signupMessage', 'Email đã tồn tại!'));
                else {
                    const uId = accountM.add(user, function(err) {
                        if (err) return done(null, false, req.flash('signupMessage', 'Đăng ký thất bại'));
                        return done(null, newUser);
                    });
                }
            })
        })
    }))
}